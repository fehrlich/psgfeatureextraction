# psgFeatureExtraction

A implementation of the [PhysiNet Challenge 2018](https://physionet.org/content/challenge-2018/1.0.0/) winning [submission](https://physionet.org/content/challenge-2018/1.0.0/papers/CinC2018-232.pdf) and their [implementation](https://github.com/matthp/Physionet2018_Challenge_Submission).


## setup

pyTorch should be installed manual with a compatible CUDA version: https://pytorch.org/get-started/locally/

### pip
`pip install -r requirements.txt`

## Run

First you should try to do a testRun, this is a complete run through all phases. This only takes a few minutes.

you can either use the `testrun.ipynb` notebook file or run:

`python -m psgFeatureExtraction testrun`

### complete

`python -m psgFeatureExtraction run`

## Data

All the downloaded and generated data will be saved to the `./data` directory. If you want to have the orginal trained models, you can just copy them from the `./dataOrg` dir to the `./data` dir.

## Run only specific steps

You can also run the diffrent steps seperate, if the data of a previous stage is needed it will run the required phases to generate the data.

- gather the data: `python -m psgFeatureExtraction gather`
- extract the data: `python -m psgFeatureExtraction prepareData`
- train the model: `python -m psgFeatureExtraction train`
- evaluate the model: `python -m psgFeatureExtraction evaluate`
