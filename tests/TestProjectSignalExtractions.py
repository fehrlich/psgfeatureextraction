import unittest
import wfdb
import numpy as np
from tests.TestEventSignal import TestEventSignal
from psgFeatureExtraction.phases.ExtractData import ExtractData

class TestProjectSignalExtractions(TestEventSignal):
    def testExtractedSignal(self):
        indexes, names = self.getIndexAndName()
        signal = self.getSignal()

        eventSignals = self.eventManager.getEventSignalFromAnnotations(indexes, names, signal.shape, "sleepStage")


        phase = ExtractData()
        preparedSignals = phase.prepareAnnotationSignals(eventSignals, self.getSignal().shape)
        preparedArousal, preparedApnea, preparedSleep = preparedSignals

        # sleep original: [0,0,2,2,4,32,32,8,8,8]
        assert all([-1,-1,0,0,1,1,1,1,1,1] == preparedSleep)

        # apnea original:
        # central = np.array([0,1,1,1,1,1,0,0,0,0])*8
        # obstr = np.array([0,0,0,1,0,0,0,0,0,0])*2
        # hypo = np.array([0,0,0,0,0,0,0,1,1,0])*16
        assert all([0,-1,-1,-1,-1,-1,0,1,1,0] == preparedApnea)

        assert all([0,0,0,0,0,0,0,0,0,0] == preparedArousal)


if __name__ == '__main__':
    unittest.main()
