import unittest
import wfdb
import numpy as np
from psgFeatureExtraction.model.Scorer import Challenge2018Score
from sklearn.metrics import cohen_kappa_score


class TestEvaluation(unittest.TestCase):
    def __init__(self, methodName):
        self.eventManager = Challenge2018Score()
        super().__init__(methodName)

    expertSignals = [
        ("original", [np.array([1, 1, 1, 1, 1, 0, 0, 0, 0, 0])]),
        ("with ignored", [np.array([1, 1, -1, 1, 1, 0, 0, -1, 0, 0]), np.array([1, 1, 1, 1, 0, 0, 0, 0])]),
    ]
    testPredictedSignals = [
        ("all correct" ,np.array([1, 1, 1, 1, 1, 0, 0, 0, 0, 0])),
        ("all incorrect" ,np.array([0, 0, 0, 0, 0, 1, 1, 1, 1, 1])),
        ("all negative" ,np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 1])),
        ("all positive" ,np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1])),
        ("bin1" ,np.array([1, 0, 0, 1, 1, 0, 0, 1, 1, 0])),
        ("bin2" ,np.array([0, 0, 1, 1, 0, 1, 0, 0, 1, 1])),
        ("bin3" ,np.array([0, 0, 1, 1, 0, 1, 0, 0, 1, 0])),
        ("bin4" ,np.array([1, 1, 1, 1, 0, 1, 1, 0, 1, 0])),
        ("bin5" ,np.array([1, 1, 1, 1, 0, 1, 1, 0, 1, 0])),
        ("pred1" ,np.array([0.4, 0.7, 0.4, 0.8, 0.5, 0.6, 0.5, 0.7, 0.4, 0.99])),
        ("pred2" ,np.array([0.8, 0.7, 0.9, 0.2, 1.0, 0.5, 0.1, 0.4, 0.1, 0.2])),
        ("pred3" ,np.array([0.3, 0.4, 0.7, 0.5, 0.8, 0.2, 0.7, 0.5, 0.2, 0.5])),
        ("pred4" ,np.array([0.5, 0.7, 0.9, 0.4, 0.4, 0.0, 1.0, 0.1, 0.9, 0.6])),
        ("pred5" ,np.array([0.9, 0.2, 0.6, 0.0, 0.1, 0.0, 0.6, 0.6, 0.2, 0.3])),
    ]

    def testKappa(self):

        for expertSignalName, expertSignals in self.expertSignals:
            for name, testPredictedSignal in self.testPredictedSignals:
                expertSignal = expertSignals[0]

                scorer = Challenge2018Score()
                scorer.score_record(truth=expertSignal, predictions=testPredictedSignal)
                scorer._auc()


                if len(expertSignals) > 1:
                    assert all(expertSignals[1] == scorer.truth), "tailored truth signal is false in %s:%s: %s == %s" % (expertSignalName, name, expertSignals[1], scorer.truth)
                    assert all(testPredictedSignal[expertSignal >= 0] == scorer.prediction), "tailored predicted signal is false in %s:%s: %s == %s" % (expertSignalName, name, testPredictedSignal[expertSignal >= 0], scorer.prediction)
                    expertSignal = expertSignals[1]
                    testPredictedSignal = scorer.getBestBinSignal(signal=scorer.prediction)
                else:
                    testPredictedSignal = scorer.getBestBinSignal()


                realKappa = cohen_kappa_score(expertSignal, testPredictedSignal)
                assert round(realKappa, 5) == round(scorer.kappa, 5), "Kappas do not match for %s:%s: %s == %s" % (expertSignalName, name, realKappa, scorer.kappa)


if __name__ == '__main__':
    unittest.main()
