import unittest
import wfdb
import numpy as np
from psgFeatureExtraction.model.PSGEventManager import PSGEventManager


class TestEventSignal(unittest.TestCase):

    def __init__(self, methodName):
        self.eventManager = PSGEventManager()
        super().__init__(methodName)

    def getIndexAndName(self):
        eventObj = {}
        eventObj["sleepStage"] = [
            (2, "W"),
            (4, "N1"),
            (5, "R"),
            (7, "N2"),
        ]
        eventObj["apnea"] = [
            (1, "(resp_centralapnea"),
            (5, "resp_centralapnea)"),
            (3, "(resp_obstructiveapnea"),
            (3, "resp_obstructiveapnea)"),
            (7, "(resp_hypopnea"),
            (8, "resp_hypopnea)"),
        ]
        indexes = list(map(lambda x: x[0], eventObj["sleepStage"] + eventObj["apnea"]))
        names = list(map(lambda x: x[1], eventObj["sleepStage"] + eventObj["apnea"]))

        return indexes, names

    def getSignal(self):
        signal = np.full(10, 1)
        signal[5] = 0

        return signal


    def getExpectedSleepSignal(self):
        return [0,0,2,2,4,32,32,8,8,8]

    def getExpetedApneaSignal(self):
        central = np.array([0,1,1,1,1,1,0,0,0,0]) * 8
        obstr = np.array([0,0,0,1,0,0,0,0,0,0]) * 2
        hypo = np.array([0,0,0,0,0,0,0,1,1,0]) * PSGEventManager.INDEX_APNEA_HYPO
        return  central + obstr + hypo

    def testEventSignalCreation(self):
        indexes, names = self.getIndexAndName()
        signal = self.getSignal()

        eventSignals = self.eventManager.getEventSignalFromAnnotations(indexes, names, signal.shape, "sleepStage")

        assert all(self.getExpectedSleepSignal() == eventSignals["sleepStage"])
        assert all(self.getExpetedApneaSignal() == eventSignals["apnea"])

    def testEventSignalFromWfdb(self):
        signal = self.getSignal()
        indexes, names = self.getIndexAndName()
        annot = wfdb.Annotation("testRecord", "none", indexes, aux_note=names)

        eventSignals = self.eventManager.getEventSignalFromWfdbAnnotations(annot, signal.shape, "sleepStage")

        assert all(self.getExpectedSleepSignal() == eventSignals["sleepStage"])
        assert all(self.getExpetedApneaSignal() == eventSignals["apnea"])

if __name__ == '__main__':
    unittest.main()
