from pyPhases.Project import Project

from pyPhases.storage.FileStorage import FileStorage
from pyPhases.exporter.ObjectExporter import ObjectExporter
from .model.Scorer import MultiClassificationScorer
from .exporter.TorchExporter import TorchExporter
from .exporter.NumpyStreamExporter import NumpyStreamExporter

from .phases.GatherPhysioNetData import GatherPhysioNetData
from .phases.ModelTrain import ModelTrain
from .phases.ModelEval import ModelEval
from .phases.ModelPredict import ModelPredict
from .phases.ExtractData import ExtractData

import sys

# Project setup
project = Project()
project.debug = False
project.testRun = False
project.name = 'psgFeatureExtraction'
project.namespace = 'tud.ibmt'

#used publisher

#used exporter
project.registerExporter(ObjectExporter())
project.registerExporter(NumpyStreamExporter({
    "basePath": "data/",
}))
project.registerExporter(TorchExporter({
    "basePath": "data/",
}))

# used storage engine
project.addStorage(FileStorage({
    "basePath": "data/",
}))

# stages
project.addStage("gather")
project.addStage("prepareData")
project.addStage("train")
project.addStage("evaluate")
project.addStage("predict", autorun=False)

# add phases
project.addPhase(GatherPhysioNetData(), "gather", "get Data from the physionet challange 2018")
project.addPhase(ExtractData(), "prepareData", "extract and prepare the psg data from the downloaded records")
project.addPhase(ModelTrain(), "train", "Load the extracted data and train the Model")
project.addPhase(ModelPredict(), "predict", "predict a outcome for a sample")
project.addPhase(ModelEval(), "evaluate", "evaluate Model")

# run all phases or a specific phase if there is an argument
if len(sys.argv) > 1 and (sys.argv[1] == "run" or sys.argv[1] == "testrun"):
    if sys.argv[1] == "testrun":
        project.testRun = True
    if project.testRun:
        project.setConfig("manualGathering", True)
        project.setConfig("dataSetVersion", "Debug")
        project.setConfig("batchSize", 5)
        project.setConfig("stopAfterNotImproving", -1)
        project.setConfig("foldModelVersions", ["Debug"])
    if(len(sys.argv) > 2):
        if "--debug" in sys.argv:
            project.debug = True
            project.run()
        else:
            project.run(sys.argv[2])
    else:
        project.run()
else:
    print("available commands: run, testrun")
