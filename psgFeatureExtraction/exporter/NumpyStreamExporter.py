import numpy
import multiprocessing as mltp
import queue
from random import Random
import gc
import os

from pyPhases.exporter.DataExporter import DataExporter
from pyPhases.Data import DataNotFound
from ..model.Model import Sleep_model_MultiTarget


# TODO: try static
def loadRecords(extractedDataPath, options, recordIndices=None):
    fp = numpy.memmap(extractedDataPath, **options)

    return numpy.array(fp[recordIndices, ::, :])

def fillBatchQueue(extractedDataPath, options, seed = None, resultQueue=None, resultSize=30):
    if seed is not None:
        rng = Random()
        rng.seed(seed)
    numRecords = options["shape"][0]

    recordList = list(range(numRecords))

    if resultQueue is None:
        if seed is not None:
            rng.shuffle(recordList)
        return loadRecords(extractedDataPath, options, recordList)

    else:
        batchCounts = numRecords//resultSize

        if batchCounts*resultSize != numRecords:  # last batch is not complete
            batchCounts += 1

        while True:
            if seed is not None:
                rng.shuffle(recordList)
            for n in range(batchCounts):
                fromIndex = (n*resultSize)
                toIndex = min(((n+1)*resultSize), numRecords)
                recordListPart = recordList[fromIndex:toIndex]
                res = loadRecords(extractedDataPath, options,
                                  recordList[(n*resultSize):((n+1)*resultSize)])
                assert len(res) == (toIndex-fromIndex)
                resultQueue.put(res)


class NumpyStreamExporter(DataExporter):
    """ Asynchrone numpy exporter """

    includesStorage = True
    batchFillProcess = None

    def initialOptions(self):
        return {
            "basePath": ".",
            "multiProcesssing": False,
            "batchTimeout": 45,
            "batchSize": 30,
            "seed": 51,
            "memmap": {}
        }

    def checkType(self, type):
        return type == numpy.memmap

    def getPath(self, dataId):
        return self.getOption("basePath") + dataId

    def stream(self, dataId, options):
        return numpy.memmap(self.getPath(dataId), **options)

    def importData(self, dataId, options):
        self.extractedDataPath = self.getPath(dataId)

        if not os.path.isfile(self.extractedDataPath):
            raise DataNotFound()

        self.memmapOptions = options["memmap"]
        self.seed = self.getOption("seed")

        # todo outsource to optionize
        if "multiProcesssing" in options:
            self.options["multiProcesssing"] = options["multiProcesssing"]
        if "batchSize" in options:
            self.options["batchSize"] = options["batchSize"]
        if "shuffle" in options and not options["shuffle"]:
            self.seed = None

        self.batchSize = self.getOption("batchSize")

        self.CurrentItemIndex = 0
        self.result = None
        self.length = self.memmapOptions["shape"][0]

        self.log("Prepare loading %s (multiprocessing: %i)" %
              (self.extractedDataPath, self.getOption("multiProcesssing")))

        if not self.getOption("multiProcesssing"):
            self.result = fillBatchQueue(
                self.extractedDataPath, self.memmapOptions, self.seed)
        else:
            self.resultManager = mltp.Manager()
            self.resultQueue = self.resultManager.Queue(maxsize=1)

            self.batchFillProcess = mltp.Process(target=fillBatchQueue, args=(
                self.extractedDataPath, self.memmapOptions, self.seed, self.resultQueue, self.batchSize))
            self.batchFillProcess.start()
        return self

    def loadNextBatch(self):
        result = None
        timeout = self.getOption("batchTimeout")
        while result is None:
            try:
                result = self.resultQueue.get(timeout=timeout)
            # empty exeption get a new batch, with new seed
            except queue.Empty as e:
                self.logWarning('Batch Filling run Empty, recreate')
                self.restartBatch()
                result = None
            except Exception as e:
                self.logError('Batch Fill hung %s'%(e))
                self.restartBatch(updateSeed=False)
                result = None

        self.result = result

    def restartBatch(self, updateSeed = True):
        if updateSeed and self.seed is not None:
            self.seed += 7
        self.result = None

        self.log("Restart Batch")
        if self.getOption("multiProcesssing"):
            self.batchFillProcess.terminate()
            # Restart everything
            self.resultManager = mltp.Manager()
            self.resultQueue = self.resultManager.Queue(maxsize=1)

            self.batchFillProcess = mltp.Process(target=fillBatchQueue, args=(self.extractedDataPath, self.memmapOptions, self.seed, self.resultQueue, self.batchSize))
            gc.collect()
            self.batchFillProcess.start()
        else:
            self.result = fillBatchQueue(self.extractedDataPath, self.memmapOptions, self.seed)

        self.CurrentItemIndex = 0

    def __iter__(self):
        self.CurrentItemIndex = 0
        return self

    def __next__(self, raiseStopIteration = True):
        if self.result is None:
            self.loadNextBatch()
            self.CurrentItemIndex = 0

        if self.getOption("multiProcesssing") and (self.CurrentItemIndex >= self.batchSize):
            self.loadNextBatch()
            self.CurrentItemIndex = 0

        if raiseStopIteration and self.CurrentItemIndex >= len(self.result):
            raise StopIteration

        itemResult = self.result[self.CurrentItemIndex]
        self.CurrentItemIndex += 1

        return itemResult

    def getNextItem(self):
        return self.__next__()

    def getNextItemAndReshuffle(self):
        try:
            return self.__next__(True)
        except StopIteration:
            self.restartBatch()
            return self.__next__(True)

    def __len__(self):
        return self.length


    def getAll(self):
        return self.result

    def close(self):
        if self.getOption("multiProcesssing"):
            self.batchFillProcess.terminate()
