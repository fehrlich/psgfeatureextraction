import numpy
import pyarrow
import torch
import io
from pandas import DataFrame

from pyPhases.exporter.DataExporter import DataExporter
from pyPhases.Data import DataNotFound
from ..model.Model import Sleep_model_MultiTarget


class TorchExporter(DataExporter):
    """ An Exporter that supports a lot of default formats using pyarrow"""

    includesStorage = True

    def initialOptions(self):
        return {
            "basePath": "."
        }

    def getPath(self, dataId):
        return self.getOption("basePath") + dataId

    def checkType(self, type):
        return issubclass(type, torch.nn.Module)

    def importData(self, dataId, options = {}):
        try:
            return torch.load(self.getPath(dataId))
        except FileNotFoundError:
            raise DataNotFound()

    def exportDataId(self, dataId, model):
        f = open(self.getPath(dataId), 'wb')
        torch.save(model, f)
        f.close()
