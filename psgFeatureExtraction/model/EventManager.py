import numpy as np
from pyPhases.util.Logger import classLogger

class EventNotListedException(Exception):
    pass

@classLogger
class EventManager():
    eventGroups: None

    def __init__(self, eventGroups):
        self.eventGroups = eventGroups

    def getEventSignalFromWfdbAnnotations(self, annotations, signalShape, defaultEventName = "sleepStage"):
        annotationNames = annotations.aux_note
        annotationIndexes = annotations.sample
        return self.getEventSignalFromAnnotations(annotationIndexes, annotationNames, signalShape, defaultEventName)

    def findEventValue(self, eventValueName):
        for _, groupName in enumerate(self.eventGroups):
            group = self.eventGroups[groupName]
            if eventValueName in group:
                eventValue = 2**group.index(eventValueName)
                return groupName, eventValue
        raise EventNotListedException

    def getEventSignalFromAnnotations(self, annotationIndexes, annotationNames, signalShape, defaultEventName = "sleepStage"):
        currentEventIndexes = {}
        lastDefaultGroupIndex = 0
        lastdefaultGroupValue = 0
        emptySignal = np.full(signalShape, 0).astype(np.int32)
        eventSignals = {}

        def getSampleIndex(annotationIndex):
            return annotationIndexes[annotationIndex]

        for annotationIndex, event in enumerate(annotationNames):
            eventValueName = event
            start = True
            add = False
            eventValue = 1
            isEventMarker = False
            eventName = None

            if event[0] == '(':
                eventValueName = event[1:]
                isEventMarker = True
            elif event[-1:] == ')':
                eventValueName = event[0:-1]
                start = False
                isEventMarker = True

            try:
                eventName, eventValue = self.findEventValue(eventValueName)
            except EventNotListedException:
                self.logError("Eventvalue %s was not listed in the specified eventgroup"%(eventValueName))
                continue

            if eventName == "ignore":
                continue

            if eventName not in eventSignals:
                eventSignals[eventName] = np.copy(emptySignal)
            if eventName not in currentEventIndexes:
                currentEventIndexes[eventName] = {}

            if start:
                if eventValueName in currentEventIndexes[eventName] and currentEventIndexes[eventName][eventValueName][0] is not None:
                    raise Exception("Event %s:%s startet twice"%(eventName, eventValueName))
                if not isEventMarker:
                    curValue = eventValue
                    currentEventIndexes[eventName][eventValueName] = [lastDefaultGroupIndex, None]

                    eventValue = lastdefaultGroupValue
                    lastDefaultGroupIndex = getSampleIndex(annotationIndex)
                    lastdefaultGroupValue = curValue
                    add = True
                else:

                    currentEventIndexes[eventName][eventValueName] = [getSampleIndex(annotationIndex), None]
            else:
                add = True

            if add:
                currentEventIndexes[eventName][eventValueName][1] = getSampleIndex(annotationIndex)

                evIndex = currentEventIndexes[eventName][eventValueName]
                if isEventMarker:
                    evIndex[1] += 1
                eventSignals[eventName][slice(evIndex[0], evIndex[1])] += eventValue
                currentEventIndexes[eventName][eventValueName] = [None, None]

        eventSignals[defaultEventName][slice(lastDefaultGroupIndex, None)] = lastdefaultGroupValue
        return eventSignals
