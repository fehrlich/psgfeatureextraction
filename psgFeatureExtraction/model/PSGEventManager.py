from .EventManager import EventManager

class PSGEventManager(EventManager):

    INDEX_WAKE = 2
    INDEX_NREM1 = 4
    INDEX_NREM2 = 8
    INDEX_NREM3 = 16
    INDEX_REM = 32

    INDEX_APNEA_OBSTRUCTIVE = 2
    INDEX_APNEA_HYPO = 4
    INDEX_APNEA_CENTRAL = 8
    INDEX_APNEA_MIXED = 16
    INDEX_APNEA_PARTIALOBSTRUCTIVE = 32
    INDEX_APNEA_HYPOVENTILATION = 64

    INDEX_AROUSAL_RERA = 2
    INDEX_AROUSAL_SPONTANEOUS = 4

    def __init__(self):
        eventGroups = {
            "sleepStage": ["undefined", "W", "N1", "N2", "N3", "R"],
            "apnea": ["None", "resp_obstructiveapnea", "resp_hypopnea", "resp_centralapnea", "resp_mixedapnea", "resp_partialobstructive", "resp_hypoventilation"],
            "arousal": ["None", "arousal_rera", "arousal_spontaneous", "arousal_plm", "arousal_snore", "resp_cheynestokesbreath", "arousal_bruxism", "arousal_noise"],
        }
        super().__init__(eventGroups)
