import random
from pyPhases.Data import Data, DataNotFound


class DataSetManager():
    recordList = None
    requestRecordMap = None
    groups = [
        "test",
        "validation",
        "training"
    ]

    slices = {}
    requestRecordMap = None
    checkIntegrity = True

    def __init__(self, recordList, seed=2):
        self.slices = {}
        self.recordList = recordList
        length = len(recordList)
        requestRecordMap = list(range(length))

        if seed > 0:
            r = random.Random()
            r.seed(seed)
            r.shuffle(requestRecordMap)

        self.requestRecordMap = requestRecordMap

    def getIndices(self):
        indices = {}

        for group in self.groups:
            indices[group] = []
            for s in self.slices[group]:
                indices[group].extend(self.requestRecordMap[s])

        return indices

    def addSlice(self, group, s):

        if not self.groups.__contains__(group):
            raise Exception("The group with the name '%s' does not exists"%(group))

        if not group in self.slices:
            self.slices[group] = []

        self.slices[group].append(s)

    def getGroupedRecords(self):
        returnTuple = tuple()

        indices = self.getIndices()
        areUnique = True

        allRecords = set()
        for group in self.groups:
            records = [self.recordList[ind] for ind in indices[group]]
            returnTuple += (records,)
            areUnique = areUnique and len(allRecords.intersection(set(records))) == 0
            allRecords = allRecords.union(records)

        isComplete = len(allRecords) == len(self.recordList)

        if self.checkIntegrity:
            assert areUnique == True
            assert isComplete == True

        return returnTuple


