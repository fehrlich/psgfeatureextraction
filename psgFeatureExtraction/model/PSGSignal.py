import numpy as np
import wfdb

from scipy.signal import fftconvolve

import matplotlib.pyplot as plt


class PSGSignal:
    channels: []
    signals: None
    annotationSignals: None
    frequency = 200

    filtCoeff = np.array([
        0.00637849379422531, 0.00543091599801427, -0.00255136650039784,
        -0.0123109503066702, -0.0137267267561505, -0.000943230632358082,
        0.0191919895027550, 0.0287148886882440, 0.0123598773891149,
        -0.0256928886371578, -0.0570987715759348, -0.0446385294777459,
        0.0303553522906817, 0.148402006671856, 0.257171285176269,
        0.301282456398562, 0.257171285176269, 0.148402006671856,
        0.0303553522906817, -0.0446385294777459, -0.0570987715759348,
        -0.0256928886371578, 0.0123598773891149, 0.0287148886882440,
        0.0191919895027550, -0.000943230632358082, -0.0137267267561505,
        -0.0123109503066702, -0.00255136650039784, 0.00543091599801427,
        0.00637849379422531
    ])

    def __init__(self, signals, annotations=None):
        self.setSignals(signals)

        if not annotations is None:
            self.setAnnotations(annotations)

    def setSignals(self, signals):
        self.signals = np.array(signals)

    def setAnnotations(self, signals):
        self.annotationSignals = np.array(signals)

    def getSignals(self):
        "get the full PSG Signals and all annotation in form of a numpy array"
        self.signals = self.signals.astype(np.float32)
        self.annotationSignals = self.annotationSignals.astype(np.float32)
        return np.concatenate((self.signals, self.annotationSignals), axis=1)

    def downsample(self,
                   targetFrequency,
                   channels=slice(0, None),
                   paddingBegin=0,
                   paddingEnd=None):
        "downsample the signal, if the signal length is not completely divisible with the frequency it will be cut off at the end"
        if (not paddingEnd is None) and paddingEnd > 0:
            paddingEnd -= paddingEnd
        signalSlice = slice(paddingBegin, paddingEnd,
                            self.frequency // targetFrequency)
        self.signals = self.signals[signalSlice, channels]

        if not self.annotationSignals is None:
            self.annotationSignals = self.annotationSignals[signalSlice, :]

    def bandpass(self):
        for n in range(self.signals.shape[1]):
            self.signals[::, n] = np.convolve(self.signals[::, n],
                                              self.filtCoeff,
                                              mode='same')

    def simpleNormalize(self, channelSlice, minMax):
        self.signals[::, channelSlice] += -32768.0
        self.signals[::, channelSlice] /= 65535.0
        self.signals[::, channelSlice] -= minMax

    def scale(self):
        center = np.mean(self.signals, axis=0)
        scale = np.std(self.signals, axis=0)
        scale[scale == 0] = 1.0
        self.signals = (self.signals - center) / scale

    def fftConvolution(self, kernel_size, channels=slice(0, None)):
        # Compute and remove moving average with FFT convolution
        resultShape = self.signals[::, channels].shape
        center = np.zeros(resultShape)
        channelIndexes = list(range(0, self.signals.shape[1]))[channels]
        for n in channelIndexes:
            center[::, n] = fftconvolve(self.signals[::, n],
                                        np.ones(shape=(kernel_size, )) /
                                        kernel_size,
                                        mode='same')

        self.signals[::, channels] = self.signals[::, channels] - center

        # Compute and remove the rms with FFT convolution of squared signal
        scale = np.ones(resultShape)
        for n in channelIndexes:
            temp = fftconvolve(np.square(self.signals[::, n]),
                               np.ones(shape=(kernel_size, )) / kernel_size,
                               mode='same')

            # Deal with negative values (mathematically, it should never be negative, but fft artifacts can cause this)
            temp[temp < 0] = 0.0

            # Deal with invalid values
            invalidIndices = np.isnan(temp) | np.isinf(temp)
            temp[invalidIndices] = 0.0
            maxTemp = np.max(temp)
            temp[invalidIndices] = maxTemp

            # Finish rms calculation
            scale[::, n] = np.sqrt(temp)

        # To correct records that have a zero amplitude chest signal
        scale[(scale == 0) | np.isinf(scale) | np.isnan(scale)] = 1.0
        self.signals[::, channels] = self.signals[::, channels] / scale

    def plot(self, signalSlice=(0, None, None), title="signal Plot"):
        wfdb.plot_items(signal=self.signals[signalSlice],
                        fs=self.frequency,
                        title='Plot',
                        time_units='seconds')
