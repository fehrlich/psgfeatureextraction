import numpy as np
import pylab
from sklearn.metrics import confusion_matrix
import pandas as pd
from ..libs.confusionMatrixPrettyPrint import pretty_plot_confusion_matrix
from sklearn.metrics import cohen_kappa_score


class Challenge2018Score:
    """Class used to compute scores for the 2018 PhysioNet/CinC Challenge.

    A Challenge2018Score object aggregates the outputs of a proposed
    classification algorithm, and calculates the area under the
    precision-recall curve, as well as the area under the receiver
    operating characteristic curve.

    After creating an instance of this class, call score_record() for
    each record being tested.  To calculate scores for a particular
    record, call record_auprc() and record_auroc().  After scoring all
    records, call gross_auprc() and gross_auroc() to obtain the scores
    for the database as a whole.
    """

    rocMap = {}
    bestF1ScoreRelu = 0
    kappa = 0
    orgTruth = None
    orgPrediction = None
    truth = None
    prediction = None
    lastRelu = 0
    signal = None

    def __init__(self, input_digits=None):
        """Initialize a new scoring buffer.

        If 'input_digits' is given, it is the number of decimal digits
        of precision used in input probability values.
        """
        if input_digits is None:
            input_digits = 3
        self._scale = 10**input_digits
        self._pos_values = np.zeros(self._scale + 1, dtype=np.int64)
        self._neg_values = np.zeros(self._scale + 1, dtype=np.int64)
        self._record_auc = {}

    def score_record(self, truth, predictions, record_name=None):
        """Add results for a given record to the buffer.

        'truth' is a vector of arousal values: zero for non-arousal
        regions, positive for target arousal regions, and negative for
        unscored regions.

        'predictions' is a vector of probabilities produced by the
        classification algorithm being tested.  This vector must be
        the same length as 'truth', and each value must be between 0
        and 1.

        If 'record_name' is specified, it can be used to obtain
        per-record scores afterwards, by calling record_auroc() and
        record_auprc().
        """
        # Check if length is correct
        if len(predictions) != len(truth):
            raise ValueError("length of 'predictions' does not match 'truth'")

        # only use non-ignored regions for evaluation
        self.orgTruth = truth
        self.orgPrediction = predictions.copy()
        self.prediction = predictions[truth >= 0]
        self.truth = truth[truth >= 0]

        # Compute the histogram of all input probabilities
        b = self._scale + 1
        r = (-0.5 / self._scale, 1.0 + 0.5 / self._scale)
        all_values = np.histogram(predictions, bins=b, range=r)[0]

        # Check if input contains any out-of-bounds or NaN values
        # (which are ignored by numpy.histogram)
        if np.sum(all_values) != len(predictions):
            raise ValueError("invalid values in 'predictions'")

        # Compute the histogram of probabilities within arousal regions
        pred_pos = predictions[truth > 0]
        pos_values = np.histogram(pred_pos, bins=b, range=r)[0]

        # Compute the histogram of probabilities within unscored regions
        pred_ign = predictions[truth < 0]
        ign_values = np.histogram(pred_ign, bins=b, range=r)[0]

        # Compute the histogram of probabilities in non-arousal regions,
        # given the above
        neg_values = all_values - pos_values - ign_values

        self._pos_values += pos_values
        self._neg_values += neg_values

        if record_name is not None:
            self._record_auc[record_name] = self._auc(pos_values, neg_values)

    def _auc(self, pos_values=None, neg_values=None):
        if pos_values is None:
            pos_values = self._pos_values
            neg_values = self._neg_values

        # Calculate areas under the ROC and PR curves by iterating
        # over the possible threshold values.

        # At the minimum threshold value, all samples are classified as
        # positive, and thus TPR = 1 and TNR = 0.
        tp = np.sum(pos_values)
        fp = np.sum(neg_values)
        tn = fn = 0
        tpr = 1
        tnr = 0
        if tp == 0 or fp == 0:
            # If either class is empty, scores are undefined.
            return (float('nan'), float('nan'))
        ppv = float(tp) / (tp + fp)
        auroc = 0
        auprc = 0

        # As the threshold increases, TP decreases (and FN increases)
        # by pos_values[i], while TN increases (and FP decreases) by
        # neg_values[i].

        self.rocMap = {}
        harmonyMax = 0
        kappaMax = -1
        kappaMaxIndex = 0
        harmonyMaxIndex = 0
        beta = 1
        index = 0

        for (n_pos, n_neg) in zip(pos_values, neg_values):
            tp -= n_pos
            fn += n_pos
            fp -= n_neg
            tn += n_neg
            tpr_prev = tpr
            tnr_prev = tnr
            ppv_prev = ppv
            tpr = float(tp) / (tp + fn)
            tnr = float(tn) / (tn + fp)
            if tp + fp > 0:
                ppv = float(tp) / (tp + fp)
            else:
                ppv = ppv_prev

            fpr = fp / (tn + fp)
            self.rocMap[fpr] = tpr
            harmony = ((1+beta**2)*tp)/((1+beta**2)*tp + beta**2*fn + fp)
            if harmony > harmonyMax:
                harmonyMax = harmony
                harmonyMaxIndex = index

            kappa_all = (tp + tn + fp + fn)
            kappa_p0 = (tp + tn) / kappa_all
            kappa_pYes = ((tp + fn)*(tp + fp)) / (kappa_all**2)
            kappa_pNo = ((tn + fn)*(tn + fp)) / (kappa_all**2)
            kappa_pe = kappa_pYes + kappa_pNo
            kappa = (kappa_p0 - kappa_pe) / (1 - kappa_pe)

            if kappa > kappaMax and index != 1000 and  index > 0:
                kappaMaxIndex = index
                kappaMax = kappa

            auroc += (tpr_prev - tpr) * (tnr + tnr_prev) * 0.5
            auprc += (tpr_prev - tpr) * ppv_prev
            index += 1

        self.f1score = harmonyMax
        self.f1scoreRelu = harmonyMaxIndex / self._scale
        self.lastAUC = auroc
        self.lastAP = auprc
        self.kappa = kappaMax
        self.kappaMaxRelu = kappaMaxIndex / self._scale

        return (auroc, auprc)

    def plotRocMap(self, titel="%s"):
        for _, val in enumerate(self.rocMap):
            pylab.plot(val, self.rocMap[val], ".b")
        pylab.ylim([0.0, 1.0])
        pylab.xlim([0.0, 1.0])
        pylab.title(titel % ('ROC-Curve AUROC: %f AUPRC: %f Kappa: %f F1: %f' %
                             (self.lastAUC, self.lastAP, self.kappa, self.f1score)))

        pylab.show()

    def getMaxKappaSignal(self):
        return self.getBestBinSignal(self.kappaMaxRelu)

    def getCorrectionSegments(self, correctSignals=False):
        correctSignal = self.getCorrectSignal()
        slices = []

        curValue = correctSignal[0]
        startIndex = 0

        for i, val in enumerate(correctSignal):
            if curValue != val:
                slices.append((curValue, slice(startIndex, i)))
                startIndex = i
                curValue = val
        return slices


    def getBestBinSignal(self, relu = None, signal = None):
        if relu is None:
            relu = self.kappaMaxRelu
        if signal is None:
            signal = self.orgPrediction

        bestSignal = np.full(signal.shape, 0)
        bestSignal[signal > relu] = 1

        return bestSignal

    def getCorrectSignal(self):
        bestSignal = self.getBestBinSignal()
        correctSignal = np.full(bestSignal.shape, 0)
        correctSignal[self.orgTruth == bestSignal] = 1
        correctSignal[self.orgTruth == -1] = -1

        return correctSignal

    def confusionMatrix(self, labels=None, relu=None):

        relu = self.kappaMaxRelu
        bestSignal = np.full(self.prediction.shape, 0)
        bestSignal[self.prediction > relu] = 1

        cm = confusion_matrix(self.truth, bestSignal)
        df_cm = pd.DataFrame(cm, range(2), range(2))

        pretty_plot_confusion_matrix(df_cm)

class MultiClassificationScorer:
    kappa = None
    predictions = []
    truth = []
    def score(self, truth, predictions, append=False):
        kappa = cohen_kappa_score(truth, predictions)
        self.kappa = kappa
        if append:
            self.truth = np.concatenate((self.truth, truth))
            self.predictions = np.concatenate((self.predictions, predictions))
        else:
            self.truth = truth
            self.predictions = predictions

    def scoreAll(self):
        self.score(self.truth, self.predictions)

    def scoreBinArrays(self, truth, predictions, append=False):
        truth = self.arrayToClassId(truth)
        predictions = self.arrayToClassId(predictions)
        return self.score(truth, predictions, append)

    def arrayToClassId(self, arrays):
        return list(map(lambda e: e.argmax(), arrays))


    def confusionMatrix(self, labels=None, size=2):

        cm = confusion_matrix(self.truth, self.predictions, labels=[0,1,2,3,4,5])
        df_cm = pd.DataFrame(cm, range(size), range(size))

        pretty_plot_confusion_matrix(df_cm)

    def getCorrectionSegments(self, correctSignals=False):
        correctSignal = self.getCorrectSignal()
        slices = []

        curValue = correctSignal[0]
        startIndex = 0

        for i, val in enumerate(correctSignal):
            if curValue != val:
                slices.append((curValue, slice(startIndex, i)))
                startIndex = i
                curValue = val
        return slices

    def getCorrectSignal(self):
        bestSignal = self.predictions
        correctSignal = np.full(bestSignal.shape, 0)
        correctSignal[self.truth == bestSignal] = 1
        # correctSignal[self.truth == -1] = -1

        return correctSignal
