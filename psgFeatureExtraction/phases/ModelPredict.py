from pyPhases import Phase
from pyPhases.Data import Data, DataNotFound

from scipy.io import loadmat
from scipy.signal import fftconvolve
import gc
import numpy as np
import torch
import os

from ..model.Model import Sleep_model_MultiTarget


class ModelPredict(Phase):
    """segment the Data depending on features

    on execution the method main is called
    you can use the phase attributes for better artifact handling
    self.config all configuration input should be stored here (p.e. self.config['epochLength'] = 30.)
    self.metrics the evaluated metrics like 'loss' or 'accuracy' (p.e. self.metrics['accuracy'] = 0.9)
    self.summary
    """

    exportData = [
        Data("predicted")
    ]

    cudaAvailable = torch.cuda.is_available()

    def __init__(self):
        self.config['foldModelVersions'] = [
            "Auxiliary1",
            "Auxiliary2",
            "Auxiliary3",
            "Auxiliary4",
        ]

    def predict(self, featureSignal, scale_factor=4, get_likelihood=False):
        classifications = self.project.getConfig("classifications")
        frequency = self.project.getConfig("frequency")
        targetFrequency = self.project.getConfig("frequency") * scale_factor
        predictions = []

        for i in range(len(classifications)):
            predictions.append(np.zeros(featureSignal.shape[0]*scale_factor))

        featureSignal = torch.Tensor(
            featureSignal).unsqueeze(0).permute(0, 2, 1)

        foldModelVersions = self.project.getConfig('foldModelVersions')
        foldingLength = len(foldModelVersions)

        for version in foldModelVersions:
            self.project.setConfig("dataSetVersion", version)

            states = self.project.getData("modelState", torch.nn.Module)

            if isinstance(states, torch.nn.Module):
                model = states
            else:
                model = Sleep_model_MultiTarget(numSignals=self.project.getConfig("channelCount"))
                model.load_state_dict(states)
            model = model.eval()

            if self.cudaAvailable:
                model = model.cuda()
                featureSignal = featureSignal.cuda()

            with torch.no_grad():
                tmpClasssPredictions = model(featureSignal)

                for i in range(len(tmpClasssPredictions)):
                    tmpClasssPredictions[i] = torch.nn.functional.interpolate(tmpClasssPredictions[0], scale_factor=targetFrequency, mode='linear', align_corners=True)
                    tmpClasssPredictions[i] = tmpClasssPredictions[i][0, ::, ::]
                    if not get_likelihood:
                        tmpClasssPredictions[i] = torch.argmax(tmpClasssPredictions[i], dim=0)
                    predictions[i] += tmpClasssPredictions[i].detach().cpu().numpy()

            # Clean up
            del tmpClasssPredictions
            del model
            torch.cuda.empty_cache()
            gc.collect()

        for i in range(len(classifications)):
            predictions[i] /= foldingLength

        return predictions

    def main(self):
        predictionMap = {}

