from pyPhases import Phase
from urllib.request import urlretrieve
import os


class GatherPhysioNetData(Phase):
    """get Data from the physionet challange 2018
    """

    exportData = ["recordIds"]

    urlPath = 'https://physionet.org/files/challenge-2018/1.0.0/training'
    filePath = 'data/training/'

    def __init__(self):
        self.config["filePath"] = self.filePath
        self.config["manualGathering"] = False

    def saveFromUrl(self, relUrlEdf: str, force=False):
        url = self.urlPath + "/" + relUrlEdf
        self.log("retreive: " + url)

        fileName = url.split('/')[-1]
        if force or not os.path.isfile(self.filePath + fileName):
            urlretrieve(url, self.filePath + fileName)

    def fromUrl(self, relUrlEdf: str, relUrlAnnotation: str = None, force=False):
        """ Description Loads a File from an url into a local file and calls addDataFromFile afterwards
        """
        annotFileName = None

        url = self.urlPath + relUrlEdf
        self.log("retreive: " + url)
        fileName = url.split('/')[-1]
        if force or not os.path.isfile(fileName):
            urlretrieve(url, self.filePath + fileName)
        if(relUrlAnnotation):
            relUrlAnnotation = self.urlPath + relUrlAnnotation
            annotFileName = relUrlAnnotation.split('/')[-1]
            if force or not os.path.isfile(annotFileName):
                urlretrieve(relUrlAnnotation, self.filePath + annotFileName)

    def getIndividualData(self, dataId="", training=True, force=False):
        basePath = dataId + "/" + dataId
        self.saveFromUrl(basePath + ".hea", force)
        self.saveFromUrl(basePath + ".mat", force)

        if training:
            self.saveFromUrl(basePath + ".arousal", force)
            self.saveFromUrl(basePath + "-arousal.mat", force)

    def main(self):
        # get a list of all training records
        self.saveFromUrl("RECORDS")
        recordIds = []

        with open(self.filePath + "RECORDS") as f:
            for line in f:
                recordId = line.split("/")[0]  # remove trailing "/\n"
                recordIds.append(recordId)

        self.project.registerData("recordIds", recordIds)

        if self.project.getConfig("manualGathering"):
            return

        # get all the record data
        for recordId in recordIds:
            self.getIndividualData(recordId)
