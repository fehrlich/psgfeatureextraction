from pyPhases import Phase
import numpy as np
import wfdb
import pylab
import math
from matplotlib.collections import LineCollection

from ..model.Scorer import MultiClassificationScorer
from .ModelPredict import ModelPredict
from sklearn.preprocessing import label_binarize

import matplotlib.pyplot as plt

class ModelEval(Phase):
    """evaluate the model
    """

    exportData = []

    def evaluateSingleRecord(self, resultArray, name="", scorers=None):
        featureSignal, eventSignals = self.prepareDataSet(resultArray)

        predictPhase = self.project.getPhase("ModelPredict")
        allPredictions = predictPhase.predict(featureSignal)
        if scorers is None:
            scorers = self.classificationSignalRange

        for i in scorers:
            prediction = allPredictions[i][::4]
            expert = eventSignals[::,i]
            expert[expert == -1] = 0
            scorer = MultiClassificationScorer()
            scorer.score(expert, prediction)
            self.log("%s %s Scores: Kappa: %f "%(name, self.classifications[i]["name"], scorer.kappa))

            scorer.confusionMatrix(size=self.classifications[i]["classcount"], labels=self.classifications[i]["classNames"])

            bestSignal = prediction

            cm = {0: "r", 1: "g", -1: "b"}
            legendNames = {0: "wrong predictions", 1: "right predictions", -1: "not scored"}

            segmentSlices = scorer.getCorrectionSegments(True)
            first = True
            legends = []
            for value, segmentSlice in segmentSlices:
                if first == False:
                    segmentSlice = slice(segmentSlice.start-1, segmentSlice.stop)
                plt.plot(list(map(lambda y: y, range(segmentSlice.start, segmentSlice.stop))), bestSignal[segmentSlice], cm[value])
                first = False
                if legendNames[value] is not None:
                    legends.append(legendNames[value])
                    legendNames[value] = None
            plt.title("Prediction for %s" % (name))
            plt.legend(legends, loc="upper right")
            plt.show()

    def evaluate(self, testData):
        scorer = []
        scoreBestWorstResults = []
        for c in self.classifications:
            scorer.append(MultiClassificationScorer())
            scoreBestWorstResults.append([(0, None), (1, None)])

        index = 0
        length = len(testData)
        for resultArray in testData:
            index += 1

            # TODO: outsource
            trainPhase = self.project.getPhase("ModelTrain")
            featureSignal, eventSignals = self.prepareDataSet(resultArray)

            predictPhase = self.project.getPhase("ModelPredict")
            self.logProgress("Predict Evaluation-Set", current=index, max=length)
            allPredictions = predictPhase.predict(featureSignal, 1)

            for i in self.classificationSignalRange:
                prediction = allPredictions[i]
                expert = eventSignals[::,i]
                expert[expert == -1] = 0
                scorer[i].score(expert, prediction, append=True)
                if scorer[i].kappa > scoreBestWorstResults[i][0][0]:
                    scoreBestWorstResults[i][0] = (scorer[i].kappa, resultArray)
                if scorer[i].kappa < scoreBestWorstResults[i][1][0]:
                    scoreBestWorstResults[i][1] = (scorer[i].kappa, resultArray)


        for i in self.classificationSignalRange:
            s = scorer[i]
            s.scoreAll()
            self.log("%s Kappa: %s"%(self.classifications[i]["name"], s.kappa))
            s.confusionMatrix(size=self.classifications[i]["classcount"], labels=self.classifications[i]["classNames"])
            del s
            self.evaluateSingleRecord(scoreBestWorstResults[i][1][1], "Worst record", [i])
            self.evaluateSingleRecord(scoreBestWorstResults[i][0][1], "Best record", [i])

    def prepareDataSet(self, resultArray):
        numChannels = self.project.getConfig('channelCount')
        addedChannels = self.project.getConfig('addedFeatureChannels')
        resultArray = resultArray.reshape(self.recordLength, numChannels+addedChannels)
        featSignal = np.array(resultArray[::, 0:numChannels])
        annotationSignal = np.array(resultArray[::, numChannels:])

        return (featSignal, annotationSignal)

    def main(self):
        self.channelRange = range(self.project.getConfig('channelCount'))
        self.classificationSignalCount = self.project.getConfig('addedFeatureChannels')
        self.classificationSignalRange = range(self.classificationSignalCount)
        self.classifications = self.project.getConfig("classifications")

        testLength = len(self.project.getData("recordListTest", list))
        self.recordLength = self.project.getConfig('recordLength')

        trainingsData = self.project.getData("testSet", np.memmap, options={
            "memmap": {
                "dtype": 'float32',
                "mode": 'r',
                "shape": (testLength, self.recordLength, self.project.getConfig('dataSetChannelCount')),
            },
            "shuffle": False,
            "batchSize": 30,
            "multiProcesssing": True
        })

        self.evaluate(trainingsData)
        trainingsData.close()
