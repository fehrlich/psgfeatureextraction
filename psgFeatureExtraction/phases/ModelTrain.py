import gc
import os
import timeit
from pathlib import Path

import numpy as np
import torch
import torch.optim as optim
from sklearn.metrics import cohen_kappa_score
from sklearn.preprocessing import label_binarize

from pyPhases import Phase
from pyPhases.Data import Data, DataNotFound

from ..model.Model import Sleep_model_MultiTarget
from ..model.Scorer import Challenge2018Score, MultiClassificationScorer

class ModelTrain(Phase):
    """Load the extracted data and train the Model
    """

    exportData = [
        Data("modelState", ["dataSetVersion"]),
    ]

    trainingData = None
    validationData = None
    dataSetVer = None
    recordLength = None
    modelPath = './data/Models/'

    def __init__(self):
        self.config['batchSize'] = 100
        self.config['stopAfterNotImproving'] = 10
        self.config['arousalLossFactor'] = 2
        self.config['binsOutput'] = 6

    def prepareDataSet(self, resultArray, tensorize = True):
        numChannels = self.project.getConfig('channelCount')
        featureChannels = self.project.getConfig('addedFeatureChannels')
        reductionFactor = self.project.getConfig("frequency")
        resultArray = resultArray.reshape(-1,self.recordLength, numChannels+featureChannels)
        feats = np.array(resultArray[::, ::, 0:numChannels])

        if tensorize:
            feats = torch.FloatTensor(feats).permute(0, 2, 1).detach().contiguous()

        targetTuple = tuple()
        for i in range(featureChannels):
            array = np.array(resultArray[::, 0::reductionFactor, numChannels+i]).astype(np.int32)
            if tensorize:
                array = torch.LongTensor(array).contiguous().view(-1)
            targetTuple += (array,)

        return (feats, targetTuple)

    def remapLabelsFromList(self, annotationTuble):
        batchArray = []
        for a in annotationTuble:
            batchArray.append(self.remapLabels(a))

        return batchArray

    def remapLabels(self, annotations, tensorize=True):
        annotations = label_binarize(annotations, classes=[-1, 1, 2, 3, 4, 5])
        return torch.LongTensor(annotations) if tensorize else annotations

    def evalValidationAccuracy(self, model):
        validationData = self.validationData.getAll()

        self.log('Predicting on Validation Data')

        features, targets = self.prepareDataSet(validationData, False)
        targets = list(targets)

        # for i in range(self.annotationChannelsCount):
        #     targets[i] = targets[i].reshape(-1)

        channelCount = self.project.getConfig('channelCount')
        features = torch.FloatTensor(features).permute(0, 2, 1).detach().contiguous()
        model.eval()

        realPredictions = []
        for i in range(self.annotationChannelsCount):
             realPredictions.append([None]*len(validationData))

        for n in range(len(validationData)):
            feats = features[n].view(1, channelCount, -1)

            # Run model
            outputs = model(feats.cuda())

            for i in range(self.annotationChannelsCount):
                # Reshape and pull back to cpu
                outputs[i] = outputs[i].data.cpu().permute(0, 2, 1).contiguous().view(-1, self.config["binsOutput"])
                # realPredictions[i][n] = np.squeeze(outputs[i][::, 1].numpy())
                realPredictions[i][n] = outputs[i]
                # Compact probability predictions

            gc.collect()

        self.log('Evaluating Validation Performance')

        scorers = []
        metrics = []

        for i in range(self.annotationChannelsCount):
            # Compact probability predictions
            realPredictions[i] = np.concatenate(realPredictions[i])
            targets[i] = np.concatenate(targets[i])

            s = MultiClassificationScorer()
            scorers.append(s)
            targets[i] = self.remapLabels(targets[i])
            s.scoreBinArrays(targets[i], realPredictions[i])
            metrics.append([s.kappa])

        return metrics


    def trainModel(self, model):

        optimizer = optim.Adam(model.parameters())

        criterions = []
        metricDefinitions = [
            # ["AUC", 0.5],
            # ["AP", 0.0],
            ["Kappa", 0.0],
        ]
        bestMetrics = []


        for i in range(self.annotationChannelsCount):
            # criterions.append(torch.nn.CrossEntropyLoss(ignore_index=-1, reduce=True).cuda())
            criterions.append(torch.nn.MultiLabelSoftMarginLoss(reduction="mean").cuda())
            bestMetrics.append([])
            for j in range(len(metricDefinitions)):
                bestMetrics[i].append(metricDefinitions[j].copy())

        i_epoch = 0
        numBatchesPerEpoch = self.project.getConfig('batchSize')
        notImprovedSince = 0
        stopAfterNotImproving = self.project.getConfig('stopAfterNotImproving')
        lastImprovdModel = None

        while True:
            # Put in train mode
            trainingStartTime = timeit.default_timer()

            model.train()
            runningLoss = 0.0

            for n in range(numBatchesPerEpoch):

                losses = []
                # Fetch pre-loaded batch from asynchronous data loader
                resultArray = self.trainingData.getNextItemAndReshuffle()
                batchFeats, annotationTuble = self.prepareDataSet(resultArray)

                targs = self.remapLabelsFromList(annotationTuble)

                # Send batch to GPU
                batchFeats = batchFeats.cuda()
                for i in range(len(targs)):
                    targs[i] = targs[i].cuda()

                # Compute the network outputs on the batch
                outputs = model(batchFeats)

                for i, c in enumerate(self.classifications):
                    # print("%s - %s"%(targs[i].shape, outputs[i].shape))
                    outputs[i] = outputs[i].permute(0, 2, 1).contiguous().view(-1, self.config["binsOutput"])

                    # Compute the losses
                    losses.append(criterions[i](outputs[i], targs[i]) * c["weight"])


                loss = sum(losses)/len(self.classifications)

                # Backpropagation
                loss.backward()

                # Perform one optimization step
                currentBatchLoss = loss.data.cpu().numpy()
                runningLoss += currentBatchLoss

                optimizer.step()
                optimizer.zero_grad()

            i_epoch += 1

            trainingEndTime = timeit.default_timer()

            # Get validation accuracy
            metricsValues = self.evalValidationAccuracy(model)

            metricStrings = []
            metricDiffStrings = []
            metricValuetrings = []
            improved = False
            modelId = 'checkpointModel_' + str(i_epoch)
            for i in range(self.annotationChannelsCount):
                for j in range(len(metricDefinitions)):
                    bMmetric = bestMetrics[i][j]
                    metricVal = metricsValues[i][j]
                    name = "%s-%s"%(self.classifications[i]["name"], bMmetric[0])
                    metricStrings.append(name + ": " + "{:.3f}".format(metricVal))
                    metricDiffStrings.append(name + ": " + "{:.3f}".format(metricVal - bMmetric[1]))
                    metricValuetrings.append("{:.3f}".format(metricVal))

                    if metricVal > bMmetric[1]:
                        improved = True
                        bMmetric[1] = metricVal


            validationEndTime = timeit.default_timer()

            self.log('////////////////////')
            self.log('Epoch Number: ' + str(i_epoch) + '  Training Time: ' + str(trainingEndTime - trainingStartTime) + '  Validation Time: ' + str(validationEndTime - trainingEndTime))
            self.log('Average Training Loss: ' + str(runningLoss / float(numBatchesPerEpoch)))
            self.log(' '.join(metricStrings))


            # If the validation accuracy improves, print out training and validation accuracy values and checkpoint the model
            if improved:
                self.log('Model Improved: ' + ' '.join(metricDiffStrings))
                f = open(self.getModelPath() + '/' + modelId + '_'.join(metricValuetrings) + '.pkl', 'wb')
                torch.save(model, f)
                f.close()
                notImprovedSince = 0
                lastImprovdModel = model
            else:
                notImprovedSince += 1
                self.log("Model not improving since %i epochs" % (notImprovedSince))

            if notImprovedSince > stopAfterNotImproving:
                self.project.registerData("modelState", lastImprovdModel)
                break

    def getModelPath(self):
        version = self.project.getConfig("dataSetVersion")
        return self.modelPath + version

    def createFolder(self):
        Path(self.getModelPath()).mkdir(parents=True, exist_ok=True)


    def main(self):
        self.annotationChannelsCount = self.project.getConfig('addedFeatureChannels')
        self.classifications = self.project.getConfig("classifications")

        if self.project.debug:
            self.config["batchSize"] = 2

        self.dataSetVer = self.project.getConfig("dataSetVersion")

        self.createFolder()
        datasetChannels = self.project.getConfig("dataSetChannelCount")
        trainingLength = len(self.project.getData("recordListTraining", list))
        validationLength = len(self.project.getData("recordListValidation", list))
        numChannels = self.project.getConfig('channelCount')
        self.recordLength = self.project.getConfig('recordLength')

        self.validationData = self.project.getData("validationSet", np.memmap, options={
            "memmap": {
                "dtype": 'float32',
                "mode": 'r',
                "shape": (validationLength, self.recordLength, datasetChannels),
            },
            "shuffle": True,
        })

        self.trainingData = self.project.getData("trainingSet", np.memmap, options={
            "memmap": {
                "dtype": 'float32',
                "mode": 'r',
                "shape": (trainingLength, self.recordLength, datasetChannels),
            },
            "multiProcesssing": True,
            "shuffle": True,
        })

        model = Sleep_model_MultiTarget(numSignals=numChannels, binClasses=[self.config['binsOutput']])
        model.cuda()
        model.train()

        self.trainModel(model)
        self.trainingData.close()
        self.validationData.close()
