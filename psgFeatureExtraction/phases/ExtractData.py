import collections
import gc
import os
import random
import math
from multiprocessing import Pool

import numpy as np
import wfdb
from scipy.io import loadmat
from scipy.signal import fftconvolve

from pyPhases import Phase
from pyPhases.Data import Data, DataNotFound

from ..model.DataSetManager import DataSetManager
from ..model.PSGEventManager import PSGEventManager
from ..model.PSGSignal import PSGSignal


class ExtractData(Phase):
    """ extract the data from original files, preprogess the data and save them into a numpy memory map
    """

    exportData = [
        Data("trainingSet", ["dataSetVersion", "channels", "recordTimeLimitInHours", "frequency", "addedFeatureChannels"]),
        Data("validationSet", ["dataSetVersion", "channels", "recordTimeLimitInHours", "frequency", "addedFeatureChannels"]),
        Data("testSet", ["dataSetVersion", "channels", "recordTimeLimitInHours", "frequency", "addedFeatureChannels"]),
        Data("recordListTraining", ["dataSetVersion"]),
        Data("recordListValidation", ["dataSetVersion"]),
        Data("recordListTest", ["dataSetVersion"]),
    ]

    channelMap = [
        'F3-M2', 'F4-M1', 'C3-M2', 'C4-M1', 'O1-M2', 'O2-M1', 'E1-M2',
        'Chin1-Chin2', 'ABD', 'CHEST', 'AIRFLOW', 'SaO2', 'ECG'
    ]

    dataSetVersions = [
        "Default", "Auxiliary1", "Auxiliary2", "Auxiliary3", "Auxiliary4",
        "Debug"
    ]

    allIds = []
    useMultiprocessing = True
    checkDataSet = True

    # needs to be direct attributes for multiprocesssing
    keepChannels = None
    filePath = None
    frequencyOriginal = None
    frequency = None

    def __init__(self):
        # Keep all channels except ECG
        self.config['channels'] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12]
        self.config["classifications"] = [{
            "name": "Arousal",
            "classcount": 2,
            "weight": 2,
            "classNames": ["No", "Yes"]
        },{
            "name": "Apnea",
            "classcount": 2,
            "weight": 1,
            "classNames": ["No", "Yes"]
        },{
            "name": "Sleep",
            "classcount": 2,
            "weight": 1,
            "classNames": ["No", "Yes"]
        }]
        self.config['channelCount'] = len(self.config['channels'])
        self.config['dataSets'] = ["Validation", "Training"]
        self.config['dataSetVersion'] = self.dataSetVersions[1]
        self.config['recordTimeLimitInHours'] = 7
        self.config['frequency'] = 50
        self.config['frequencyOriginal'] = 200
        self.config['frequencyQuotient'] = self.config['frequencyOriginal'] / self.config['frequency']
        self.config['addedFeatureChannels'] = len(self.config["classifications"])
        self.config['dataSetChannelCount'] = self.config['addedFeatureChannels'] + self.config['channelCount']
        self.config['targetSignalType'] = "float32"
        self.config['recordLength'] = self.config['recordTimeLimitInHours'] * 3600 * self.config['frequency']

        self.config['numRecordsPerBatch'] = os.cpu_count() - 1
        self.config['startWithBatchNumber'] = 0  # for debug purpose only

        self.frequencyOriginal = self.config['frequencyOriginal']
        self.frequency = self.config["frequency"]

    def loadSignals(self, recordName):
        filePath = self.filePath
        try:
            signals = loadmat(filePath + recordName + '.mat')
        except:
            self.logWarning("Coudn't load record '%s', try to reload" %(recordName))
            self.project.getPhase("GatherPhysioNetData").getIndividualData(recordName, force=True)
            signals = loadmat(filePath + recordName + '.mat')

        signals = signals['val']
        gc.collect()

        return signals

    def extractAnnotationsSignals(self, recordName, targetShape):
        annotation = wfdb.rdann(self.filePath + recordName, 'arousal')
        eventSignal = PSGEventManager().getEventSignalFromWfdbAnnotations(annotation, targetShape)

        return self.prepareAnnotationSignals(eventSignal, targetShape)

    def prepareAnnotationSignals(self, eventSignal, targetShape):

        sleepStageAnnotations = np.full(targetShape, 0).astype(np.int16)
        if "sleepStage" in eventSignal:
            sleepStageAnnotations = np.array(list(map(lambda x: 0 if x == 0 else math.log(x, 2), eventSignal["sleepStage"])))

            sleepStageAnnotations[eventSignal["sleepStage"] == 0] = -1 # undefined
            sleepStageAnnotations[eventSignal["sleepStage"] == 1] = -1 # explicit undefined

        return (sleepStageAnnotations,)

    def extractRecordAndAnnotations(self, recordName):
        return self.extractRecord(recordName, True)

    def extractRecord(self, recordName, extractAnnotations=False):
        rawSignals = self.loadSignals(recordName)

        rawSignals = np.transpose(rawSignals).astype(np.float64)
        signalLength = rawSignals.shape[0]

        signal = PSGSignal(rawSignals)
        signal.frequency = self.frequencyOriginal

        if extractAnnotations:
            annotationArr = self.extractAnnotationsSignals(recordName, signalLength)
            signal.setAnnotations(np.transpose(annotationArr))

        signal.bandpass()
        signal.downsample(self.frequency, channels=self.keepChannels)

        # Scale SaO2 to sit between -0.5 and 0.5, a good range for input to neural network
        signal.simpleNormalize(11, 0.5)
        signal.scale()

        # Normalize all the other channels by removing the mean and the rms in an 18 minute rolling window, using fftconvolve for computational efficiency
        # 18 minute window is used because because baseline breathing is established in 2 minute window according to AASM standards.
        # Normalizing over 18 minutes ensure a 90% overlap between the beginning and end of the baseline window
        kernel_size = (self.frequency * 18 * 60) + 1
        o2Excluded = slice(0, -1)
        signal.fftConvolution(kernel_size, o2Excluded)

        gc.collect()

        return signal.getSignals()

    def getRecordLists(self):

        assert len(self.allIds) == 994
        dataSetVersion = self.project.getConfig("dataSetVersion")

        self.log("get dataset version: %s" % (dataSetVersion))
        dm = DataSetManager(self.allIds, 29)

        if dataSetVersion == 'Default':
            dm.addSlice("test", slice(0, 200))
            dm.addSlice("validation", slice(894, None))
            dm.addSlice("training", slice(200, 894))
        elif dataSetVersion == 'Auxiliary1':
            dm.addSlice("test", slice(0, 100))
            dm.addSlice("validation", slice(100, 200))
            dm.addSlice("training", slice(200, None))
        elif dataSetVersion == 'Auxiliary2':
            dm.addSlice("test", slice(0, 100))
            dm.addSlice("validation", slice(300, 400))
            dm.addSlice("training", slice(100, 300))
            dm.addSlice("training", slice(400, None))
        elif dataSetVersion == 'Auxiliary3':
            dm.addSlice("test", slice(0, 100))
            dm.addSlice("validation", slice(600, 700))
            dm.addSlice("training", slice(100, 600))
            dm.addSlice("training", slice(700, None))
        elif dataSetVersion == 'Auxiliary4':
            dm.addSlice("test", slice(0, 100))
            dm.addSlice("validation", slice(894, None))
            dm.addSlice("training", slice(100, 894))
        elif dataSetVersion == 'Debug':
            dm.addSlice("test", slice(0, 2))
            dm.addSlice("validation", slice(200, 205))
            dm.addSlice("training", slice(300, 310))
        else:
            raise Exception("dataSetVersion %s is not supported" %(dataSetVersion))

        dm.checkIntegrity = self.checkDataSet
        return dm.getGroupedRecords()

    def getData(self, datasetName):
        "the getData message is called if a other phase needs data that is declared in the self.exportData attribute"

        if self.project.testRun:
            self.checkDataSet = False

        self.keepChannels = self.config['channels']
        self.filePath = self.project.getConfig("filePath")
        self.allIds = self.project.getData("recordIds", list)

        testRecordList, validationRecordList, trainRecordList = self.getRecordLists()

        if datasetName == 'recordListTraining':
            self.project.registerData("recordListTraining", trainRecordList)
            return trainRecordList
        if datasetName == 'recordListValidation':
            self.project.registerData("recordListValidation", validationRecordList)
            return validationRecordList
        if datasetName == 'recordListTest':
            self.project.registerData("recordListTest", testRecordList)
            return testRecordList
        if datasetName == 'trainingSet':
            recordList = trainRecordList
            validationRecordList = None
        elif datasetName == 'validationSet':
            recordList = validationRecordList
            trainRecordList = None
        elif datasetName == 'testSet':
            recordList = testRecordList
            trainRecordList = None
        else:
            raise Exception('Invalid dataset name: %s' % (datasetName))

        batchIndex = 0
        sampleDataLimit = self.config["recordTimeLimitInHours"] * 3600 * self.config['frequency']
        numRecordsPerBatch = self.config["numRecordsPerBatch"]
        startWithBatchNumber = self.config['startWithBatchNumber']

        if self.useMultiprocessing:
            pool = Pool(processes=numRecordsPerBatch)

        recordShape = (len(recordList), sampleDataLimit, self.config['dataSetChannelCount'])
        fp = self.project.registerStream(datasetName, np.memmap, {
            "dtype": 'float32',
            "mode": 'w+',
            "shape": recordShape
        })

        # Extract data in batches of numRecordsPerBatch
        batchIndexes = range(startWithBatchNumber * numRecordsPerBatch, len(recordList), numRecordsPerBatch)
        batchCount = len(batchIndexes)
        lengthFactor = 3600 * self.config['frequency']
        self.log("%s Extracting %i Batches of the shape %s (Multiprocessing: %i)" % (datasetName, batchCount, recordShape, self.useMultiprocessing))

        for n in batchIndexes:
            self.log('Extracting Batch: %i/%i ' % (batchIndex + 1, batchCount))
            # Compute upper limit for this batch of records to extract
            limit = n + numRecordsPerBatch
            if (n + numRecordsPerBatch) > len(recordList):
                limit = len(recordList)

            # Process and extract batch of records
            if self.useMultiprocessing:
                data = pool.map(self.extractRecordAndAnnotations, recordList[n:limit])
            else:
                data = list(map(self.extractRecordAndAnnotations, recordList[n:limit]))

            # Enforce dataLimitInHours hour length with chopping / zero padding for memory usage stability and effficiency in cuDNN
            for n in range(len(data)):
                originalLength = data[n].shape[0] / lengthFactor
                if data[n].shape[0] < sampleDataLimit:
                    # Zero Pad
                    neededLength = sampleDataLimit - data[n].shape[0]
                    extension = np.zeros(shape=(neededLength, data[n].shape[1]))
                    extension[::, -3::] = -1.0
                    data[n] = np.concatenate([data[n], extension], axis=0)

                elif data[n].shape[0] > sampleDataLimit:
                    # Chop
                    data[n] = data[n][0:sampleDataLimit, ::]

                self.log('Original Length: ' + str(originalLength) + '  New Length: ' + str(data[n].shape[0] / lengthFactor))

            gc.collect()
            self.log('Saving Batch: %s' % (str(batchIndex + 1)))

            # Save batch of extracted records to extracted data path
            for m in range(len(data)):
                fp[(batchIndex * numRecordsPerBatch) + m, ::, ::] = data[m][::, ::]
                assert isinstance(fp, np.memmap)
                fp.flush()

            gc.collect()
            batchIndex += 1

        del fp

    def main(self):
        self.getData("trainingSet")
        self.getData("validationSet")
        self.getData("recordListTraining")
        self.getData("recordListValidation")
